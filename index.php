<?php
require_once "./code.php"; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S01 Activity</title>
</head>
<body>
  <br>
  <center><h1>Full Address</h1></center>
  <center><p><?php echo getFullAddress('3F Caswynn Bldg.,Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?></p></center>
  <center><p><?php echo getFullAddress('3F Enzo Bldg.,Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?></p></center>
  <br>

  <center><h1>Letter-Based Grading</h1></center>
  <center><p>87 is equivalent to <?php echo getLetterGrade(87)?></p></center>
  <center><p>94 is equivalent to <?php echo getLetterGrade(94)?></p></center>
  <center><p>74 is equivalent to <?php echo getLetterGrade(74)?></p></center>
  <center><p>101 is equivalent to <?php echo getLetterGrade(101)?></p></center>



</body>
</html>